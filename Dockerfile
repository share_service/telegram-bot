FROM python:3.9.1

WORKDIR /usr/src/app

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

RUN apt update

RUN pip install --upgrade pip
RUN pip install poetry==1.1.4
COPY pyproject.toml /usr/src/app/pyproject.toml
RUN poetry install

COPY . /usr/src/app/.
