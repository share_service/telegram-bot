import logging
import requests
import os

from telegram import Update
from telegram.ext import Updater, CommandHandler, CallbackContext

logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO
)

logger = logging.getLogger(__name__)


AUTH_TOKEN = os.environ.get("AUTH_TOKEN")
BOT_TOKEN = os.environ.get("BOT_TOKEN")
URL = "https://kitbucket.ru/api/goal_party/{0}/link_telegram_chat/"


def help_command(update: Update, _: CallbackContext) -> None:
    update.message.reply_text('/link X - link current chat to party, X - id of party, see URL')


def link(update: Update, _: CallbackContext) -> None:
    try:
        goal_party_id = int(update.effective_message.text.split()[-1])
    except ValueError:
        update.message.reply_text("Invalid data")

    url = URL.format(goal_party_id)
    data = {
        "telegram_chat_id": update.effective_chat.id,
        "username": update.effective_user.name
    }
    headers = {
        "Authorization": f"Token {AUTH_TOKEN}"
    }
    r = requests.post(url=url, json=data, headers=headers)
    if r.status_code == 200:
        update.message.reply_text("Chat linked to Party")
    else:
        update.message.reply_text("Something went wrong")


def main() -> None:
    updater = Updater(BOT_TOKEN)

    dispatcher = updater.dispatcher

    dispatcher.add_handler(CommandHandler("link", link))
    dispatcher.add_handler(CommandHandler("help", help_command))

    updater.start_polling()

    updater.idle()


if __name__ == '__main__':
    main()
